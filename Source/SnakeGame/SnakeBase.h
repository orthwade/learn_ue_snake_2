// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <array>

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection { UP, DOWN, LEFT, RIGHT };

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly)
	float ElemStep;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	const float& m_cell_dim = ElemStep;

	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	
	UPROPERTY()
	EMovementDirection CurrentMovementDirection;

	UPROPERTY()
	EMovementDirection NextMovementDirection;

	EMovementDirection GetReverseMovementDirection(EMovementDirection _direction);

	enum class EMovement { DISCRETE, SMOOTH };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector GetNextShift(const EMovementDirection& _direction);

	EMovement m_movement_type{ EMovement::DISCRETE };

	const float m_move_time_step = 0.5f;
	float m_move_timer = 0.0f;

public:
	UFUNCTION(BlueprintCallable, Category = "Misc")
	inline ASnakeElementBase* GetHead() { return SnakeElements[0]; }

	inline float GetMoveTimeStep() const { return m_move_time_step; }

	UFUNCTION(BlueprintCallable, Category = "Misc")
	inline TArray<ASnakeElementBase*>& GetArrSnakeElement() { return SnakeElements; }
	
	inline float GetCellDim() const { return m_cell_dim; }

	inline EMovement GetMovementType() const { return m_movement_type;  }

	inline EMovementDirection GetCurrentMovementDirection() const { return CurrentMovementDirection; }

	void SetCurrentMovementDirection(const EMovementDirection& _direction);
	inline void SetNextMovementDirection(const EMovementDirection& _direction) { NextMovementDirection = _direction; }

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(size_t ElementsCount = 1);

	void Move();
	void Move(float DeltaTime);

	void SnakeElementOverlap(ASnakeElementBase* SnakeElement, AActor* Other);

	void TakeHit(size_t _hit_points = 1);

	UFUNCTION(BlueprintCallable, Category = "Misc")
	void Kill();
};

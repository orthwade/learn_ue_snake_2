// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <array>

#include "SnakeBase.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"


class UCameraComponent;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

protected:

	const std::array<std::pair<FString, EMovementDirection>, 4> m_arr_pair_key_name_movement_direction
	{
		std::make_pair(FString(TEXT("A"))	, EMovementDirection::LEFT),
		std::make_pair(FString(TEXT("W"))	, EMovementDirection::UP),
		std::make_pair(FString(TEXT("D"))	, EMovementDirection::RIGHT),
		std::make_pair(FString(TEXT("S"))	, EMovementDirection::DOWN)
	};

	UPROPERTY()
	UInputComponent* m_input;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool IsNextDirectionAllowed(EMovementDirection _current, EMovementDirection _next) const;

	void SetSnakeNextMovementDirection(FKey key);
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

};

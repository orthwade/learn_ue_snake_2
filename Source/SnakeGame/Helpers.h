#pragma once

#include <vector>
#include <array>
#include <list>

template<class T>
inline bool Contains(const std::vector<T> &_vec, const T &_elem)
{ return std::find(std::begin(_vec), std::end(_vec), _elem) != std::end(_vec); }

template<class T, size_t N>
inline bool Contains(const std::array<T, N> &_arr, const T &_elem)
{ return std::find(std::begin(_arr), std::end(_arr), _elem) != std::end(_arr); }

template<class T>
inline bool Contains(const std::list<T> &_list, const T &_elem)
{ return std::find(std::begin(_list), std::end(_list), _elem) != std::end(_list); }
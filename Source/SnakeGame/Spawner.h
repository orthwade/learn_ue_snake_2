// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.h"
#include "Helpers.h"
#include "Spawner.generated.h"

UENUM(BlueprintType)
enum class ESpawn : uint8 { FOOD, HIT };

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()

	
public:	
	using coord_t = std::pair<float, float>;

	// Sets default values for this actor's properties
	ASpawner();

protected:
	ASnakeBase* Snake;

	std::array<std::array<coord_t, 9>, 9> m_matrix_pos;
	std::vector<coord_t> m_vec_pos;
	std::list<coord_t> m_list_spawned_pos;

	std::array<ESpawn, 10> m_arr_spawnable
	{
		ESpawn::FOOD,
		ESpawn::FOOD,
		ESpawn::FOOD,
		ESpawn::HIT,
		ESpawn::HIT,
		ESpawn::HIT,
		ESpawn::HIT,
		ESpawn::HIT,
		ESpawn::HIT,
		ESpawn::HIT
	};

	size_t m_arr_spawnable_index = 0;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	float Timeout;

	float m_timer;

public:	
	UFUNCTION(BlueprintCallable, Category = "Misc")
	void SetSnake(UPARAM(ref)AActor* const _snake);

	UFUNCTION(BlueprintCallable, Category = "Misc")
	void ClearPos(FVector pos);

	inline bool ApproximatelySame(float v_0, float v_1) { return std::abs(v_0 - v_1) < 0.1f; }
	inline bool Greater(float v_0, float v_1) { return !ApproximatelySame(v_0, v_1) && v_0 > v_1; }

	inline bool Stored(const coord_t& _coord) 
	{ return Contains(m_list_spawned_pos, _coord); }
	
	bool Intersect(ASnakeElementBase* _element, const coord_t& _coord);
	bool Intersect(const coord_t& _coord);

	bool CloseToHead(const coord_t& _coord) const;

	void SortPrioritizingPositionsInFrontOfSnakeHead(std::vector<coord_t>& _vec);

	void Spawn();
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSpawnCallbackDelegate, FVector, Param1Type, ESpawn, Param2Type);
	UPROPERTY(BlueprintAssignable, Category = "SpawnerCallbacks")
	FSpawnCallbackDelegate OnSpawnCallback;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

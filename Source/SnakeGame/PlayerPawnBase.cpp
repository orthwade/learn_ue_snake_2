// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
	:
	CameraComponent(CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera")))
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CameraComponent;
	AutoPossessPlayer = EAutoReceiveInput::Player0;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90.0f, 0.0f, 0.0f));

	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool APlayerPawnBase::IsNextDirectionAllowed(EMovementDirection _current, EMovementDirection _next) const
{
	bool result = true;

	switch (_next)
	{
	case EMovementDirection::UP:
		result = _current != EMovementDirection::DOWN;
		break;
	case EMovementDirection::DOWN:
		result = _current != EMovementDirection::UP;
		break;
	case EMovementDirection::LEFT:
		result = _current != EMovementDirection::RIGHT;
		break;
	case EMovementDirection::RIGHT:
		result = _current != EMovementDirection::LEFT;
		break;
	default:
		break;
	}

	return result;
}

void APlayerPawnBase::SetSnakeNextMovementDirection(FKey _key)
{
	FString string = _key.GetFName().ToString();

	FString message = TEXT("");

	message.Append(TEXT("Pressed key "));
	message.Append(string);

	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, message);

	for (const std::pair<FString, EMovementDirection>& pair : m_arr_pair_key_name_movement_direction)
	{
		if (pair.first == string)
		{
			EMovementDirection direction_current = SnakeActor->GetCurrentMovementDirection();
			EMovementDirection direction_next = pair.second;

			if (IsNextDirectionAllowed(direction_current, direction_next))
			{
				message = TEXT("");
				message.Append(TEXT("Next Direction is "));
				message.Append(string);
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, message);

				SnakeActor->SetNextMovementDirection(pair.second);

				if(SnakeActor->GetMovementType() == ASnakeBase::EMovement::DISCRETE)
					SnakeActor->SetCurrentMovementDirection(pair.second);
			}
			else
			{
				message = TEXT("");
				message.Append(TEXT("Direction "));
				message.Append(string);
				message.Append(TEXT(" is not allowed"));

				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, message);
			}

			break;
		}
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("SetNextMovementDirection", EInputEvent::IE_Pressed, this, &APlayerPawnBase::SetSnakeNextMovementDirection);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}


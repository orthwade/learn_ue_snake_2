// Fill out your copyright notice in the Description page of Project Settings.



#include "Spawner.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

#include <algorithm>

// Sets default values
ASpawner::ASpawner()
	:
	Timeout(4.0f),
	m_timer(-1.0f)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	std::random_shuffle(m_arr_spawnable.begin(), m_arr_spawnable.end());
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASpawner::SetSnake(UPARAM(ref)AActor* const _snake)
{
	if (IsValid(Snake))
		return;

	ASnakeBase* snake = Cast<ASnakeBase>(_snake);

	if (IsValid(snake))
	{
		m_vec_pos.clear();

		Snake = snake;

		Timeout = Snake->GetMoveTimeStep() * 4.0f;

		const float cell_dim = Snake->GetCellDim();

		const float float_x_0 = -cell_dim * 4.0f;
		const float float_y_0 = -cell_dim * 4.0f;

		float float_x = 0.0f;
		float float_y = 0.0f;

		for (size_t x = 0; x < m_matrix_pos.size(); ++x)
		{
			for (size_t y = 0; y < m_matrix_pos[x].size(); ++y)
			{
				float_x = float_x_0 + x * cell_dim;
				float_y = float_y_0 + y * cell_dim;

				const coord_t coord = std::make_pair(float_x, float_y);

				m_matrix_pos[x][y] = coord;

				m_vec_pos.push_back(coord);
			}
		}

		m_timer = 0.0f;
	}
}

void ASpawner::ClearPos(FVector pos)
{
	for (std::list<coord_t>::const_iterator iter = std::begin(m_list_spawned_pos); iter != std::end(m_list_spawned_pos); ++iter)
	{
		if ((*iter).first == pos.X && (*iter).second == pos.Y)
		{
			m_list_spawned_pos.erase(iter);
			break;
		}
	}
}

bool ASpawner::Intersect(ASnakeElementBase* _element, const coord_t& _coord)
{
	const float abs_d_x = std::abs(_element->GetPos0().X - _coord.first);
	const float abs_d_y = std::abs(_element->GetPos0().Y - _coord.second);

	if (abs_d_x > 0.1f || abs_d_y > 0.1f)
		return false;
	else
		return true;
}

bool ASpawner::Intersect(const coord_t& _coord)
{
	for (ASnakeElementBase* element : Snake->GetArrSnakeElement())
		if (IsValid(element) && Intersect(element, _coord))
			return true;

	return false;
}

bool ASpawner::CloseToHead(const coord_t &_coord) const
{
	const float cell_dim = Snake->GetCellDim();

	const ASnakeElementBase* head = Snake->GetArrSnakeElement()[0];

	const FVector& head_pos = head->GetPos0();

	float dx = std::abs(_coord.first - head_pos.X);
	float dy = std::abs(_coord.second - head_pos.Y);

	return dx < cell_dim * 3.0f && dy < cell_dim * 3.0f;
}

void ASpawner::SortPrioritizingPositionsInFrontOfSnakeHead(std::vector<coord_t>& _vec)
{
	EMovementDirection direction = Snake->GetCurrentMovementDirection();

	ASnakeElementBase* head = Snake->GetArrSnakeElement()[0];

	const float snake_head_x = head->GetPos0().X;
	const float snake_head_y = head->GetPos0().Y;

	std::vector<coord_t>& vec_pos = _vec;

	std::list<coord_t> list_pos{};

	for (size_t i = 1; i < vec_pos.size(); ++i)
	{
		switch (direction)
		{
		case EMovementDirection::RIGHT:
			if (ApproximatelySame(vec_pos[i].first, snake_head_x) && Greater(vec_pos[i].second, snake_head_y))
				list_pos.push_front(vec_pos[i]);
			else
				list_pos.push_back(vec_pos[i]);

			break;

		case EMovementDirection::LEFT:
			if (ApproximatelySame(vec_pos[i].first, snake_head_x) && Greater(snake_head_y, vec_pos[i].second))
				list_pos.push_front(vec_pos[i]);
			else
				list_pos.push_back(vec_pos[i]);

			break;

		case EMovementDirection::UP:
			if (ApproximatelySame(vec_pos[i].second, snake_head_y) && Greater(vec_pos[i].first, snake_head_x))
				list_pos.push_front(vec_pos[i]);
			else
				list_pos.push_back(vec_pos[i]);

			break;

		case EMovementDirection::DOWN:
			if (ApproximatelySame(vec_pos[i].second, snake_head_y) && Greater(snake_head_x, vec_pos[i].first))
				list_pos.push_front(vec_pos[i]);
			else
				list_pos.push_back(vec_pos[i]);

			break;

		default:
			break;
		}
	}

	std::vector<coord_t> vec_buffer(std::begin(list_pos), std::end(list_pos));

	vec_pos.swap(vec_buffer);
}

void ASpawner::Spawn()
{
	ESpawn enum_spawn = m_arr_spawnable[m_arr_spawnable_index];

	std::vector<coord_t> vec_pos = m_vec_pos;
	
	std::random_shuffle(std::begin(vec_pos), std::end(vec_pos));

	if (enum_spawn == ESpawn::HIT)
		SortPrioritizingPositionsInFrontOfSnakeHead(vec_pos);

	size_t i = 0;

	while (Stored(vec_pos[i]) || CloseToHead(vec_pos[i]) || Intersect(vec_pos[i]))
	{
		i = (i + 1) % vec_pos.size();

		if (i == 0)
		{
			GEngine->AddOnScreenDebugMessage(-1, 4.0f, FColor::Yellow, TEXT("No place to spawn."));
			return;
		}
	}

	const coord_t& coord = vec_pos[i];

	m_list_spawned_pos.push_back(coord);

	FVector pos{ Snake->GetArrSnakeElement()[0]->GetPos() };

	pos.X = coord.first;
	pos.Y = coord.second;

	OnSpawnCallback.Broadcast(pos, m_arr_spawnable[m_arr_spawnable_index]);

	++m_arr_spawnable_index;

	if (m_arr_spawnable_index >= m_arr_spawnable.size())
	{
		std::random_shuffle(m_arr_spawnable.begin(), m_arr_spawnable.end());

		m_arr_spawnable_index = 0;
	}

	GEngine->AddOnScreenDebugMessage(-1, 4.0f, FColor::Yellow, TEXT("Spawning..."));
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(Snake) && m_timer >= 0.0f)
	{
		m_timer += DeltaTime;

		if (m_timer >= Timeout)
		{
			m_timer = 0.0f;

			Spawn();
		}
	}
}


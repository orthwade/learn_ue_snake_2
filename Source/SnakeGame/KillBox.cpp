// Fill out your copyright notice in the Description page of Project Settings.


#include "KillBox.h"
#include "SnakeBase.h"

void AKillBox::Interact(AActor* _other, bool b_is_head)
{
	ASnakeBase* snake = Cast<ASnakeBase>(_other);

	if (IsValid(snake))
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Orange, TEXT("Snake hit killbox. End Game."));
		snake->Kill();
	}
}
